import numpy as np

def make_movie():
    import os
    import matplotlib.pyplot as plt
    import numpy as np

    # Fixing random state for reproducibility
    np.random.seed(19680801)


    files = []

    fig, ax = plt.subplots(figsize=(5, 5))
    for i in range(50):  # 50 frames
        plt.cla()
        plt.imshow(np.random.rand(5, 5), interpolation='nearest')
        fname = '_tmp%03d.png' % i
        print('Saving frame', fname)
        plt.savefig(fname)
        files.append(fname)

    print('Making movie animation.mpg - this may take a while')
    os.system("rm movie.mp4")

    os.system("ffmpeg -r "+str(10)+" -i  _tmp%03d.png movie.mp4")
    os.system("rm _tmp*.png")

def update_a(x, v, m, a):
    rcut = 400
    eps = 0.1
    sigma = 1.2

    a[:, :] = 0.0

    for i, xi in enumerate(x):
        for j, xj in enumerate(x):
            if i == j:
                continue
            rijvec = xi-xj
            rij = np.linalg.norm(rijvec)
            if rij <= rcut:
                sig_per_rij = sigma/rij
                a[i, :] += 48.0*eps/m/rij**2*(sig_per_rij**12-0.5*sig_per_rij**6)*rijvec[:]

def main():

    # init
    tmax = 10
    n = 2
    m = 1
    dt = 1
    dtsqrd = dt**2
    hlf_dt_sqrd = 0.5*dtsqrd
    half_dt = 0.5*dt
    x = np.array([[i, j, 0] for i in range(n) for j in range(n)])
    v = np.array([[0, 0, 0] for i in range(n) for j in range(n)])
    a = np.array([[0, 0, 0] for i in range(n) for j in range(n)])

    # main loop
    for t in range(tmax):
        v = v + half_dt*a
        x = x + dt*v
        update_a(x, v, m, a)
        v = v + half_dt*a
        print(x)#,v,a)

    # make_movie()
main()
